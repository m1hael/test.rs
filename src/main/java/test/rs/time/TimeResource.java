package test.rs.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test/time")
public class TimeResource {

    private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss SSS");
    
    private SharedContextRegistrar service;
    
    void bindSharedContext(SharedContextRegistrar service) {
        this.service = service;
    }
    
    void unbindSharedContext(SharedContextRegistrar service) {
        this.service = null;
    }
    
    @GET
    public String get() {
        return dateFormat.format(new Date());
    }
    
}
