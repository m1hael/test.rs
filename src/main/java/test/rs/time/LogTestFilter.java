package test.rs.time;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogTestFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Override
    public void destroy() {
        logger.info("Filter gets destroyed");
    }

    @Override
    public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain) throws IOException,
                    ServletException {
        logger.info("Preprocessing request");
        chain.doFilter(arg0, arg1);
        logger.info("Postprocessing request");
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        logger.info("Filter is initialized");
    }

}
