package test.rs.time;

import java.util.Hashtable;

import org.ops4j.pax.web.service.WebContainer;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SharedContextRegistrar {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private WebContainer webContainer;
    private BundleContext context;
    private ServiceRegistration<HttpContext> serviceRegistration;
    
    void activate(BundleContext context) {
        this.context = context;
        registerHttpContext();
    }
    
    void deactivate() {
        context.ungetService(serviceRegistration.getReference());
    }
    
    void bindWebContainer(WebContainer webContainer) {
        this.webContainer = webContainer;
    }
    
    private void registerHttpContext() {
        HttpContext httpContext = webContainer.getDefaultSharedHttpContext();
        
        Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("httpContext.id", "shared");
        props.put("httpContext.shared", "true");
        serviceRegistration = context.registerService(HttpContext.class, httpContext, props);
       
        logger.info("Shared context 'shared' installed");
    }
}
