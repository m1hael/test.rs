# Test Project for REST Services with shared HttpContext
This projects tests the usage of servlet, filters, http contexts and rest services.

## Components

### Shared Context
The shared HttpContext (id: _shared_) is registered in _activate_ method of the 
SharedContextRegistrar. The SharedContextRegistrar is registered as an OSGi 
service and is referenced by those servlets which need a shared HttpContext.

It registers a shared HttpContext (id: _shared_) as an OSGi service.

    WebContainer service = (WebContainer) context.getService(serviceReference);
    
    HttpContext httpContext = service.getDefaultSharedHttpContext();
    
    Hashtable<String, String> props = new Hashtable<String, String>();
    props.put("httpContext.id", "shared");
    props.put("httpContext.shared", "true");
    httpContextReg = context.registerService(HttpContext.class, httpContext, props);

### Servlet using no HttpContext
The _DateServlet_ is registered as a _javax.servlet.Servlet_ via DS with the 
alias _/test/date_.

### Filter using no HttpContext
The _LogTestFilter_ is registered as a _javax.servlet.Filter_ via DS. It outputs 
a log message before and after processing the HTTP request. It intercepts the 
request of the _DateServlet_.

### Servlet using HttpContext shared
The _TimestampServlet_ is registered as a _javax.servlet.Servlet_ via DS with 
the alias _/test/timestamp_ using the HttpContext _shared_.

### Filter using HttpContext shared
The _SharedLogTestFilter_ is registered as a _javax.servlet.Filter_ via DS. 
It outputs a log message before and after processing the HTTP request. It uses 
the HttpContext _shared_.

### REST Service using HttpContext shared
The _TimeResource_ is registered as an OSGi service with the interface 
_java.lang.Object_. This will be picked up by the Amdatu Web bundles and will 
register an Apache Wink servlet for the REST resource, 
see _org.amdatu.web.rest_ bundle.

## Amdatu Web
Amdatu Web uses Apache Wink for REST services. It registers a Wink servlet for each OSGi service with the correspondent service properties, see _org.amdatu.web.rest.wink.WinkRegistrationServiceImpl_.

The source has been changed to also forward the _httpContext.id_ service property.

## Setup
- download and install Apache Karaf
- install the features: http http-whiteboard scr
- copy the bundles from test.rs/bundles to the deploy folder
- build the project and install the bundles

## Testing
The project can be tested with making HTTP request to the HTTP service f. e. with curl:

    curl -v http://localhost:8181/test/date
    curl -v http://localhost:8181/test/time
    curl -v http://localhost:8181/test/timestamp

The log file should have a two log statements (pre and post) for each request.

